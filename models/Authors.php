<?php

namespace app\models;


use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property string $username
 * @property string $password
 */
class Authors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'authors';
    }
    public static function findByUsername($username)
    {
        return self::findOne(['username'=>$username]);
    }
    public static function validatePassword($password){
        return self::findOne(['password'=>$password]);
        
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'password' => 'Password',
        ];
    }
}
