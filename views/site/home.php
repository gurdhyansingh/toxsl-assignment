<?php

/* @var $this yii\web\View */

$this->title = 'Crud Using Yii Framework';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Toxsl Assignment-1</h1>

        <p class="lead">Basic Crud Operations</p>

        <p><a class="btn btn-lg btn-success" href="">Add Employe</a></p>
    </div>

    <div class="body-content">

        <div class="row">
        <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">ID</th>
                <th scope="col">Full Name</th>
                <th scope="col">Salary</th>
                <th scope="col">Address</th>
                <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr class="table-active">
                    <th scope="row">Active</th>
                    <td>Column content</td>
                    <td>Column content</td>
                    <td>Column content</td>
                    <td>
                        
                        <a href=""><button class="btn btn-info">View</button></a>
                        <a href=""><button class="btn btn-success">Update</button></a>
                        <a href=""><button class="btn btn-danger">Delete</button></a>
                    </td>
                    </tr>
                <tr>

            </tbody>
        </table>
        </div>

    </div>
</div>
