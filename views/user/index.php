<?php

use yii\helpers\Html;
use yii\grid\GridView;
$session = Yii::$app->session;
$session->open();
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <?php 

           
            
    ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'salary',
            'address',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    
    $session->close();
    $session->destroy();
    ?>


</div>
